moodle-enrol_profilefield
=========================

Allows binding an enrollment strategy to user profile values.

This is a modified version of the plugin that matches values with RegEx and also allows matching the email address.
