<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Adds new instance of enrol_paypal to specified course
 * or edits current instance.
 *
 * @package    enrol_profilefield
 * @category   enrol
 * @author     2017 Federico Vera (fedevera@unc.edu.ar)
 * @copyright  2010 Valery Fremaux (valery.fremaux@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
header('Content-Type: text/html; charset=UTF-8');
// Capabilities.
$string['profilefield:config'] = 'Puede configurar la matriculación por campos de perfil';
$string['profilefield:enrol'] = 'Puede matricular utilizando campos de perfil';
$string['profilefield:manage'] = 'Puede agregar matriculación por campos de perfil';
$string['profilefield:unenrol'] = 'Puede desmatricular cuando fue matriculado por campos de perfil';
$string['profilefield:unenrolself'] = 'Puede desmatricularse a si mismo cuando fue matriculado por campos de perfil';

$string['assignrole'] = 'Asignar rol';
$string['auto_desc'] = 'Este grupo fue creado automáticamente por medio del plugin de matricualción por campos de perfil. Será eliminado si elimina el plugin del curso.';
$string['badprofile'] = 'La información de su perfil no permite que se matricule en este curso. Sin embargo, si considera que debería poder inscribirse, contáctese con el administrador.';
$string['course'] = 'Curso : $a';
$string['enrol/profilefield:unenrolself'] = 'Puede desmatricularse a si mismo del curso';
$string['auto'] = 'Automático';
$string['auto_help'] = 'Si está activo el usuario será matriculado en curso cuando inicia sesión, sin necesidad de entrar al curso.';
$string['enrolenddate'] = 'Finalización';
$string['enrolenddate_help'] = 'Si está activo, los usuarios solo pueden matricularse hasta esta fecha.';
$string['enrolenddaterror'] = 'La fecha de finalización no puede ser anterior a la fecha de inicio';
$string['enrolme'] = 'Matricularme en el curso';
$string['enrolmentconfirmation'] = 'Bienvenido. La información de su perfil permite que se matricule en este curso. ¿Continuar?';
$string['enrolname'] = 'Matriculación por Campos de Perfil';
$string['enrolperiod'] = 'Duración de la matricualción';
$string['enrolperiod_desc'] = 'Duración predeterminada en la cual la matricualción es válida (en segundos). Si es cero, el tiempo se considera ilimitado.';
$string['enrolperiod_help'] = '';
$string['enrolstartdate'] = 'Inicio';
$string['enrolstartdate_help'] = 'Si está activo, los usuarios solo pueden matricularse apartir de esta fecha.';
$string['emptyfield'] = 'No {$a}';
$string['groupon'] = 'Agrupar por';
$string['g_none'] = 'Sin agrupamiento, o seleccione...';
$string['g_auth'] = 'Método de autenticación';
$string['g_dept'] = 'Departmento';
$string['g_inst'] = 'Institución';
$string['g_lang'] = 'Idioma';
$string['groupon_help'] = 'Este método permite matriculación directa en un curso en función de un campo de un perfil de usuario.';
$string['grouppassword'] = 'Contraseña de grupo, si la conoce.';
$string['newcourseenrol'] = 'Un nuevo participante se matriculó en el curso {$a}';
$string['nonexistantprofilefielderror'] = 'Este campo no está definido en las extensiónes de perfil de usuario';
$string['notificationtext'] = 'Plantilla de notificación';
$string['notificationtext_help'] = 'El contenido del mail puede escribirse aquí, usando las etiquetas &lt;%%USERNAME%%&gt;, &lt;%%COURSE%%&gt;, &lt;%%URL%%&gt; y &lt;%%TEACHER%%&gt;.';
$string['notifymanagers'] = '¿Avisar a los administradores?';
$string['passwordinvalid'] = 'Contraseña inválida';
$string['pluginname'] = 'Matriculación por Campos de Perfil';
$string['pluginname_desc'] = 'Este método permite matriculación directa en un curso en función de un campo de un perfil de usuario';
$string['profilefield'] = 'Campo de perfil de usuario';
$string['profilefield_desc'] = 'Un puntero a un campo de perfil de usuario';
$string['profilevalue'] = 'Expresión regular';
$string['profilevalue_desc'] = '';
$string['status'] = 'Permitir usar el perfil para la matricualción';
$string['unenrolself'] = 'Desmatricularse del curso "{$a}"?';
$string['unenrolselfconfirm'] = '¿Realmente desea desmatricularse del curso "{$a}"?';

$string['email'] = 'Email';

$string['defaultnotification'] = '
Estimado <%%TEACHER%%>,

el nuevo usuario <%%USERNAME%%> se matriculó (cumpliendo con los campos de perfil) en el curso <%%COURSE%%>.

Puede revisar este perfil desde <a href="<%%URL%%>">aquí</a> después de iniciar sesión.
';
